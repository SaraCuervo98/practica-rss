<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="estilo.css"/>
            </head>
            <body>
                <xsl select="rss">
                    <xsl:for-each select="grupo/disco">
                        <div class="contenedora contenedora1">
                            <div class="item1">
                                <xsl:element name="img">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="portada/@src"/>
                                    </xsl:attribute>
                                </xsl:element>
                                <div id="titulo">
                                    <xsl:value-of select="titulo"/>
                                </div>
                                <div id="año1">
                                    <xsl:value-of select="descripcion/año"/>
                                </div>
                            </div>
                            <div class="item2">
                                <div>
                                    <div id="genero">
                                        <text>GENERO</text>
                                        <xsl:value-of select="descripcion/genero"/>
                                    </div>
                                    <div id="estilo">
                                        <text>ESTILO</text>
                                        <xsl:value-of select="descripcion/estilo"/>
                                    </div>
                                    <div id="año2">
                                        <text>AÑO</text>
                                        <xsl:value-of select="descripcion/año"/>
                                    </div>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <th colspan="3">
                                    CANCION
                                            </th>
                                            <th colspan="3">
                                    DURACION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion1/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion1/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion2/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion2/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion3/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion3/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion4/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion4/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion5/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion5/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion6/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion6/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion7/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion7/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion8/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion8/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion9/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion9/duration"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion10/titulo"/>
                                            </td>
                                            <td colspan="3">
                                                <xsl:value-of select="canciones/cancion10/duration"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <xsl:element name="img" class="d-block w-100">
                                                <xsl:attribute name="src">
                                                    <xsl:value-of select="imagenes/imagen1/path"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="height">40%</xsl:attribute>
                                            </xsl:element>
                                        </div>
                                        <div class="carousel-item">
                                            <xsl:element name="img" class="d-block w-100">
                                                <xsl:attribute name="src">
                                                    <xsl:value-of select="imagenes/imagen2/path"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="height">40%</xsl:attribute>
                                            </xsl:element>
                                        </div>
                                        <div class="carousel-item">
                                            <xsl:element name="img" class="d-block w-100">
                                                <xsl:attribute name="src">
                                                    <xsl:value-of select="imagenes/imagen3/path"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="height">40%</xsl:attribute>
                                            </xsl:element>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                </xsl>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>